FROM alpine:latest

RUN apk update
RUN apk --no-cache add bash nano openssh-client rsync inotify-tools lsyncd duplicity postgresql-client py3-paramiko mariadb-client rdiff-backup

ENTRYPOINT ["/bin/sh"]